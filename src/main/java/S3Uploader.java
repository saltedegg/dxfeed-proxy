import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;

public class S3Uploader {

    // Artibutes
    private static final Log LOG = LogFactory.getLog(S3Uploader.class);

    private static final String awsAccessKey = "AKIAJSQQPPN44S4APHMQ";
    private static final String awsSecretKey = "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";
    private static final AWSCredentials awsCredentials = new AWSCredentials(awsAccessKey, awsSecretKey);
    private static final S3Service s3Service = new RestS3Service(awsCredentials);

    private String BUCKET_NAME = null;

    private static final S3Uploader singleton = new S3Uploader();


    // Methods
    private S3Uploader(){}

    public static S3Uploader getSingleton(){

        return singleton;

    }

    public void init(String bucket_name){

        System.out.println("Uploader is going to " + bucket_name);
        this.BUCKET_NAME = bucket_name;
        try {
            s3Service.createBucket(bucket_name);
        }catch(S3ServiceException e){
            LOG.error(e);
        }

    }

    public void upload(String jsonstring){

        if(this.BUCKET_NAME == null){
            LOG.error("S3 Bucket name has not been inited");
        }

        String mill_suffix = Long.toString(System.currentTimeMillis());
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String fileName = date + "/dxfeed_realtime_" + mill_suffix + ".txt";
        try {

            S3Object s3Object = new S3Object(fileName, jsonstring);
            s3Service.putObject(this.BUCKET_NAME, s3Object);


        }catch (NoSuchAlgorithmException e) {
            LOG.error(e);
        } catch (IOException e) {
            LOG.error(e);
        } catch (S3ServiceException e) {
            LOG.error(e);
        }

    }

}
