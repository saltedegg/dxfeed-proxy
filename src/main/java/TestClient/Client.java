package TestClient;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.CharsetUtil;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * Created by Benjamin on 17.11.2017.
 */
public class Client {

    //port and host to connect to
    final private String host;
    final private int port;

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void start_client() {

        EventLoopGroup nioGroup = new NioEventLoopGroup();
        InetSocketAddress address = new InetSocketAddress(host, port);

        Bootstrap bootstrap = new Bootstrap();

        // assign non blocking event group to client and set channel and server address
        bootstrap.group(nioGroup)
                .channel(NioSocketChannel.class)
                // client uses remote address instead of local address
                .remoteAddress(address)

        //initialize channel
                .handler(new ChannelInitializer<SocketChannel>() {
                    //add handler to pipeline on initialize
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                socketChannel.pipeline().addLast(new SimpleChannelInboundHandler<ByteBuf>() {
                    @Override
                    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
                        cause.printStackTrace();
                    }

                    @Override
                    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
                        System.out.println("Connected");
                    }

                    @Override
                    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
                        System.out.println("Disconnected");
                    }

                    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf message) throws Exception {
                        System.out.println(message.toString(CharsetUtil.UTF_8));
                    }

                });
            }
        });

        try {
            //client uses connect instead of bind
            ChannelFuture f = bootstrap.connect().sync();
            f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
