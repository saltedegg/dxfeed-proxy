import com.dxfeed.api.DXEndpoint;
import com.dxfeed.api.DXFeed;
import com.dxfeed.api.DXFeedEventListener;
import com.dxfeed.api.DXFeedSubscription;
import com.dxfeed.event.market.Quote;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.CharsetUtil;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin on 16.11.2017.
 */
public class Server {
    DXFeed feed;
    private final int port;
    private List<ChannelHandlerContext> clients = new ArrayList<ChannelHandlerContext>();
    ObjectWriter writer = new ObjectMapper().writer().withDefaultPrettyPrinter();

    // AWS S3 uploader
    S3Uploader s3Uploader = S3Uploader.getSingleton();
    String bucketName = "dxfeed-realtime-raw-data";

    public Server(int port) {
        this.port = port;
    }

    public void start_server() {
        EventLoopGroup nioGroup = new NioEventLoopGroup();
        InetSocketAddress address = new InetSocketAddress(port);

        //bootstrap server

        ServerBootstrap bootstrap = new ServerBootstrap();

        // assign non blocking event group to server and set channel and address

        bootstrap.group(nioGroup)
                .channel(NioServerSocketChannel.class)
                .localAddress(address)

                // Channel initializer

                .childHandler(new ChannelInitializer<SocketChannel>() {
                    protected void initChannel(SocketChannel socketChannel) throws Exception {

                        //Handler manages events, for example the event triggered when a client connects

                        socketChannel.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                            @Override
                            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
                                cause.printStackTrace();
                            }

                            @Override
                            public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
                                System.out.println("Client connected");
                                clients.add(ctx);
                            }

                            @Override
                            public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
                                System.out.println("Client disconnected");
                                clients.remove(ctx);
                            }
                        });
                    }
                });

        initialize_dx_feed_connection();

        try {
            ChannelFuture f = bootstrap.bind().sync();
            f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initialize_dx_feed_connection() {
        feed = DXEndpoint.create()
                .user("demo").password("demo")
                .connect("demo.dxfeed.com:7300")
                .getFeed();
        s3Uploader.init(bucketName);
        DXFeedSubscription<Quote> subscription = feed.createSubscription(Quote.class);
        subscription.addEventListener(new DXFeedEventListener<Quote>() {
            public void eventsReceived(List<Quote> list) {
                read_dx_feed_data(list);
            }
        });
        subscription.addSymbols("EUR/USD");
        subscription.addSymbols("GBP/USD");
    }

    // triggered when new quotes received
    private void read_dx_feed_data(List<Quote> list) {

        for (Quote quote: list) {
            try {
                String quote_as_json = writer.writeValueAsString(quote);
                s3Uploader.upload(quote_as_json);
            }catch(JsonProcessingException e){
                e.printStackTrace();
            }
        }

        for (ChannelHandlerContext client : clients) {
            for (Quote quote : list) {
                //System.out.println(quote.toString() + " sent to " + client.toString());
                try {
                    client.writeAndFlush(Unpooled.copiedBuffer(writer.writeValueAsString(quote), CharsetUtil.UTF_8));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
